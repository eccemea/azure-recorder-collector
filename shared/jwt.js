let jwt = require('azure-ad-jwt');

/*
    To manually verify id_token:

    To get token certifiate, get first id_token and check it's kid value (you can use https://jwt.io/ for this)
    You'll get something simillar to:
    
    {
        "typ": "JWT",
        "alg": "RS256",
        "kid": "u4OfNFPHwEBosHjtrauObV84LnY" <-- It's your Key for Certificate Key/Vlaue pair
    }

    Next, read tenantId (held in tid key/value pair) and call:
    https://login.windows.net/{tenantId}/.well-known/openid-configuration

    read jwks_uri key and GET it's URL to obtain certificates, like
    https://login.windows.net/{tenantId}/discovery/keys

    Certificate String is located inside x5c key, for matching x5t key.

    Use certificate (public key) to verify token.

*/

/*
    Validates received from Browser token. Every time jwt library download public key from Azure storage https://login.windows.net/{tenantId}/discovery/keys, 
    sign token and verify it. 
*/
async function validate(_context, _token) {
    // decode & verify token   
    return new Promise(function (resolve, reject) {
        try {
            // VALIDATE_AZURE_TOKEN is used for local tests only
            if (process.env["VALIDATE_AZURE_TOKEN"] == "0") {
                _context.log('VALIDATE_AZURE_TOKEN = false');
                resolve(true);
                return
            }
            jwt.verify(_token, null, function (err, result) {
                if (result) {
                    console.log("JWT is valid");
                    resolve(true);
                } else {
                    _context.log("JWT is invalid: " + err);
                    resolve(false)
                }
            });
        } catch (error) {
            _context.log(error);
            resolve(false)
        }
    });
}


module.exports = {
    validate
};

