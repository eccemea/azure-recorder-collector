let platformClient = require('purecloud-platform-client-v2');
const defer = require('promise-defer');

let client = platformClient.ApiClient.instance;

let clientId = process.env["clientId"];
let clientSecret = process.env["clientSecret"];
let env = process.env["env"];


// set PureCloud environment
client.setEnvironment(env);

// Login to the PureCloud and retreive Users and Queues
function login(context) {
    context.log('PureCloud auth ---');
    context.log(`env         : ${env}`);
    context.log(`clientId    : ${clientId}`);
    context.log(`clientSecret: ${clientSecret.substring(0, 10)}(...)`);

    return new Promise(function (resolve, reject) {
        try {

            client.loginClientCredentialsGrant(clientId, clientSecret)
                .then(function () {
                    context.log('service is connected to PureCloud!');

                    let resp = {
                        token: client.authData.accessToken,
                        expiryTime: client.authData.tokenExpiryTime
                    }
                    client.setAccessToken(resp.token);
                    context.log(`received token ${resp.token.substring(0, 10)}(...)`);
                    resolve(resp);

                })
                .catch(function (err) {
                    context.log(err);
                    reject();
                });

        } catch (err) {
            context.log(err);
            reject();
        }
    });
}

// Set token for future API calls
function setToken(tokenValue) {
    client.setAccessToken(tokenValue);
}

// Retreive all queueIds from PureCloud and keep those in arrQueues object
async function getQueues(_context, _divisionIds, _pageSize, _pageNumber, _def, _arrQueues) {
    _context.log(`getQueues Ids page: ${_pageNumber}`);
    if (!_def) {
        _arrQueues = [];
    }
    var deferred = _def || new defer();

    try {

        let apiInstance = new platformClient.RoutingApi();

        var opts = {
            'pageSize': _pageSize,
            'pageNumber': _pageNumber,
            'divisionId': _divisionIds
        };

        apiInstance.getRoutingQueues(opts).then(function (resp) {
            resp.entities.forEach(function (item) {
                _arrQueues.push(item.id);
            });
            if (_pageNumber >= resp.pageCount) {
                _context.log(`found ${_arrQueues.length} queues`);
                if (_arrQueues.length > 0) {
                    _context.log(`queueIds: ${_arrQueues}`);
                }
                deferred.resolve(_arrQueues);
            } else {
                getQueues(_context, _divisionIds, _pageSize, _pageNumber + 1, deferred, _arrQueues);
            }


        }).catch(function (err) {
            _context.log(err);
            deferred.reject(err);
        });
    } catch (err) {
        _context.log(err);
        deferred.reject({
            status: 400,
            body: {
                "error": err.toString()
            }
        })

    }
    return deferred.promise;
};

// Retreive all divisionIds and match them to the received names. Only those that match are kept
async function getDivisions(_context, _names) {
    _context.log(`getDivisions Ids for following names: ${_names}`)
    var arrDivisionIds = [];
    return new Promise(function (resolve, reject) {
        try {

            let apiInstance = new platformClient.ObjectsApi();
            let opts = {
                'pageSize': 50,
                'pageNumber': 1
            };

            apiInstance.getAuthorizationDivisions(opts)
                .then((resp) => {
                    resp.entities.forEach(function (item) {
                        if (_names.indexOf(item.name) > -1) {
                            arrDivisionIds.push(item.id);
                        }
                    });
                    _context.log(`match divisionIds: ${arrDivisionIds}`);
                    resolve(arrDivisionIds)
                })
                .catch((err) => {
                    _context.log('There was a failure calling getAuthorizationDivisions');
                    _context.log(err);
                    reject(err)
                });

        } catch (error) {
            _context.log(error);
            reject({
                status: 400,
                body: {
                    "error": error.toString()
                }
            })
        }
    });

};

// Call AnalyticsConversationDetails API query
async function getAnalyticsConversationDetails(_context, _interval, _queueIds, _page) {
    _context.log(`getAnalyticsConversationDetails for page: ${_page}`);
    return new Promise(function (resolve, reject) {
        try {
            let apiInstance = new platformClient.AnalyticsApi();
            // Prepare predicates
            var arrPredicates = [];
            for (var i in _queueIds) {
                arrPredicates.push({
                    "type": "dimension",
                    "dimension": "queueId",
                    "operator": "matches",
                    "value": _queueIds[i]
                })
            }


            let body = {
                "interval": _interval,
                "order": "asc",
                "orderBy": "conversationStart",
                "paging": { "pageSize": 50, "pageNumber": _page },
                "segmentFilters": [
                    {
                        "type": "and",
                        "clauses": [
                            {
                                "type": "and",
                                "predicates": [
                                    {
                                        "type": "dimension",
                                        "dimension": "mediaType",
                                        "operator": "matches",
                                        "value": "voice"
                                    }
                                ]
                            },
                            {
                                "type": "or",
                                "predicates": arrPredicates
                            }
                        ]
                    }
                ]

            }


            apiInstance.postAnalyticsConversationsDetailsQuery(body)
                .then((data) => {
                    _context.log(`postAnalyticsConversationsDetailsQuery success!`);
                    resolve(data);
                    if (!data.conversations) {
                        _context.log(`received 0 conversations`);
                    } else
                        _context.log(`received ${data.conversations.length} conversations`);

                })
                .catch((err) => {
                    _context.log('there was a failure calling postAnalyticsConversationsDetailsQuery');
                    _context.log(err);
                    reject(err);
                });
        } catch (err) {
            _context.log(error);
            reject({
                status: 400,
                body: {
                    "error": err.toString()
                }
            });
        }

    });

}

// Retreive media information about selected conversation id (recordingId / fileStatus)
async function getRecordingMediaInformation(_context, _conversationId) {
    _context.log(`getRecordingMediaInformation for conversationId ${_conversationId}`);

    return new Promise(function (resolve, reject) {

        try {
            let opts = {
                'maxWaitMs': 5000, // Number | The maximum number of milliseconds to wait for the recording to be ready. Must be a positive value.
                'formatId': "NONE" // String | The desired media format. Possible values: NONE, MP3, WAV, or WEBM

            };
            let apiInstance = new platformClient.RecordingApi();

            apiInstance.getConversationRecordingmetadata(_conversationId)
                .then((data) => {
                    _context.log(`getConversationRecordings success!`);
                    _context.log(data);
                    resolve(data);

                })
                .catch((err) => {
                    _context.log('there was a failure calling getConversationRecordings');
                    _context.log(err);
                    reject(err);
                });
        } catch (error) {
            _context.log(error);
            reject({
                status: 400,
                body: {
                    "error": err.toString()
                }
            });
        }


    });

}

// Retreive HTTPS link to the recordings. Only service is authenticated to download this recording
async function getConversationRecordingUrl(_context, _conversationId, _recordingId, _attemptNo, _def) {
    _context.log(`getConversationRecordingUrl mediaFile for conversationId ${_conversationId} and recordingId ${_recordingId} - attemptNo: [${_attemptNo}]`);
    var deferred = _def || new defer();
    try {

        let apiInstance = new platformClient.RecordingApi();

        let opts = {
            'formatId': "mp3", // String | The desired media format. WEBM was a default & works only in Chrome (no Safari)
            'download': true, // Boolean | requesting a download format of the recording            
            'filename': "audio"
        };

        apiInstance.getConversationRecording(_conversationId, _recordingId, opts)
            .then(function (data) {
                _context.log(`getConversationRecording success!`);
                _context.log(data); // disabled log as mediaUri is a long text. Re-enable in case of troubleshooting

                if (data.mediaUris) {
                    // Assign Recording File
                    if (data.media == 'audio') {
                        deferred.resolve({
                            url: data.mediaUris.S.mediaUri,
                            media: data.media,
                            fileName: `${data.conversationId}_${data.id}`
                        }
                        ); // audio
                    } else if (data.media == 'screen') {
                        deferred.resolve({
                            url: data.mediaUris["0"].mediaUri,
                            media: data.media,
                            fileName: `${data.conversationId}_${data.id}`
                        }); // screen
                    } else {
                        deferred.reject({
                            status: 404,
                            body: {
                                "error": "cannot find audio / screen media for selected conversation."
                            }
                        })
                    }


                } else {
                    if (_attemptNo > 3) {
                        _context.log('failed to get recording file 3 times. abort');
                        deferred.reject({
                            status: 400,
                            body: {
                                "error": "cannot retreive recording file."
                            }
                        })
                    } else {
                        // if recording was not requested for a time being, it must be first transcoded by PureCloud services. It result with 200 OK empty response. API call
                        // to get recording URL should be re-send after small amount of time.
                        _context.log('audioFile not yet available - retry in 2 sec');
                        setTimeout(function () {
                            getConversationRecordingUrl(_context, _conversationId, _recordingId, _attemptNo + 1, deferred);
                        }, 2000);
                    }

                }

            })
            .catch(function (err) {
                _context.log('There was a failure calling getConversationRecording');
                _context.log(err);
                deferred.reject(err);

            });
    } catch (error) {
        _context.log(error);
        deferred.reject({
            status: 400,
            body: {
                "error": error.toString()
            }
        })

    }
    return deferred.promise;
}


module.exports = {
    login,
    setToken,
    getDivisions,
    getQueues,
    getAnalyticsConversationDetails,
    getRecordingMediaInformation,
    getConversationRecordingUrl
};
