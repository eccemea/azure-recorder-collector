let azure = require('azure-storage');
let stream = require('stream');
let request = require('request').defaults({ encoding: null });
let uuidv1 = require('uuid/v1');

let tableService = azure.createTableService();
let fileService = azure.createFileService();

let tableName = 'purecloud';
let fileShare = 'recordings';

/*
    Initialization for table and fileShare used to keep PureCloud recordings. 
    AZURE_STORAGE_CONNECTION_STRING env is used to connect to the Azure storage. If objects (table/fileShare) doesn't exists , those are created
    automatically.
    Table `purecloud` keep latest PureCloud token.
    
*/
async function init(context) {
    context.log('database initializaition...');
    return new Promise(function (resolve, reject) {
        try {
            tableService.createTableIfNotExists(tableName, function (error, result, response) {
                if (!error) {                    
                    if (result.isSuccessful && result.created) {
                        context.log('new table created in azure-storage, tableName: purecloud');
                    }                    
                    //#region ** Do not create fileServie - we do not save anything on Azure anymore    
                    resolve();       
                    /*
                    fileService.createShareIfNotExists(fileShare, function (error, result, response) {
                        if (!error) {
                            if (result.created) {
                                context.log(`new fileShare created in azure-storage, shareName: ${fileShare}`);
                            }
                            resolve();
                        } else {
                            context.log(error);
                            reject(error);
                        }
                    });
                    */
                    //#endregion

                } else {
                    context.log(error);
                    reject(error);
                }
            });
        } catch (error) {
            context.log(error);
            reject({
                status: 400,
                body: {
                    "error": error.toString()
                }
            });
        }
    });
}
/*
    Retreive from table PureCloud token, to avoid multiple login attempts. Token expire date is validated, in case of < 10 min, false value is returned, and function
    executed new login request, after successfull operation, new token value is saved to table.
*/
async function getToken(context) {
    context.log('getToken PureCloud...');
    return new Promise(function (resolve, reject) {
        try {
            tableService.retrieveEntity(tableName, 'token', 'purecloud', function (error, result, response) {
                if (!error) {
                    // verify expiryDate for token
                    let diffInMinutes = Math.round((result.expiryTime["_"] - new Date().getTime()) / 60 / 1000);
                    context.log(`token valid for next ${diffInMinutes} minutes`);
                    if (diffInMinutes < 10) {
                        context.log('PureCloud token is expired');
                        resolve();
                    } else {
                        resolve({
                            token: result.value["_"],
                            expiryTime: result.expiryTime["_"]
                        });
                    }
                } else {
                    if (response.statusCode == 404) {
                        context.log('PureCloud token not present');
                        resolve()
                    } else {
                        context.log(response);
                        reject();
                    }
                }
            });
        } catch (error) {
            context.log(error);
            reject();
        }
    });
}
/*
    New token value from PureCloud is saved in Azure table.
*/
async function updateToken(context, _input) {
    context.log('save token to the database...');
    return new Promise(function (resolve, reject) {
        try {
            var entGen = azure.TableUtilities.entityGenerator;
            var entity = {
                PartitionKey: entGen.String('token'),
                RowKey: entGen.String('purecloud'),
                value: entGen.String(_input.token),
                expiryTime: entGen.String(_input.expiryTime)
            };
            tableService.insertOrReplaceEntity(tableName, entity, function (error, result, response) {
                if (!error) {
                    // result contains the entity with field 'taskDone' set to `true`
                    resolve();
                }
            });
        } catch (error) {
            context.log(error);
            reject();
        }
    });
}
/*
    Function download from PureCloud servers recording file and share it to browser
*/
async function saveMediaFile(context, _recording) {
    context.log('func saveMediaFile');
    return new Promise(function (resolve, reject) {

        try {
            context.log('download mediaFile from purecloud...');
            request.get(_recording.url, function (err, res, rawFile) {
                if (!err) {
                    context.log('file downloaded, save it to azure-storage platform...');
                    var fileStream = new stream.Readable();
                    fileStream.push(rawFile);
                    fileStream.push(null);
                                    
                    const fileBuffer = Buffer.from(rawFile, 'base64');

                    resolve(fileBuffer);
                    // The rest of the code is no longer used. We do not save anything on AzureStorage
                    return

                    //#region Save media on AzureStorage (not used)
                    
                    let uniqueFileName = `${uuidv1()}_${_recording.fileName}`;
                    context.log(`unique fileName ${uniqueFileName}`);
                    
                    fileService.createFileFromStream(fileShare, '', uniqueFileName, fileStream, rawFile.length, function (error, result, response) {
                        if (!error) {
                            // file uploaded

                            context.log('create sasToken & download link to the file...');
                            // create SAS token for 15 min 
                            var startDate = new Date();
                            var expiryDate = new Date(startDate);

                            expiryDate.setMinutes(startDate.getMinutes() + 15);
                            const sasToken = fileService.generateSharedAccessSignature(fileShare, '', uniqueFileName, {
                                AccessPolicy: {
                                    Permissions: azure.FileUtilities.SharedAccessPermissions.READ,
                                    Start: startDate,
                                    Expiry: expiryDate
                                }
                            });
                            // create download link with sas token
                            let downloadLink = fileService.getUrl(fileShare, '', uniqueFileName, sasToken);
                            context.log(`download link: ${downloadLink}`);
                            resolve(downloadLink);
                        } else {
                            context.log(error);
                            reject(error)
                        }
                    });
                    //#endregion
                    
                } else {
                    reject(err)
                }
            });
        } catch (error) {
            context.log(error);
            reject({
                status: 400,
                body: {
                    "error": error.toString()
                }
            });
        }
    });
}



module.exports = {
    init,
    updateToken,
    getToken,
    saveMediaFile
};
