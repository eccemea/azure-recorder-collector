/*
    FunctionName: getConversations
    Based on input parameters return conversations lists that match received input. Function filters results to voice interactions only with optionally
    screenshare items.
*/

let pc = require('../shared/purecloud');
let db = require('../shared/db');
let jwt = require('../shared/jwt');


module.exports = async function (context, req) {
    context.log('getRecordings started');

    let divisions = req.body.divisions;
    let interval = req.body.interval;
    let page = req.body.page;

    if (!page)
        page = 1

    if (!req.body.token || !divisions || !interval) {
        return {
            status: 400,
            body: {
                "error": "one or more input params is missing. required params are (divisions, interval, token)"
            }
        };
    }

    context.log('Input params -----');
    context.log(`id_token    : ${req.body.token.substring(0, 10)}(...)`);
    context.log(`divisions   : ${divisions}`);
    context.log(`interval    : ${interval}`);
    context.log(`page        : ${page}`);

    let bToken = await jwt.validate(context, req.body.token);
    if (!bToken) {
        return {
            status: 400,
            body: {
                "error": "your AzureAD token is invalid."
            }
        };
    }
    context.log(`azureAD token is valid. continue`);


    // initialize azure databaze
    await db.init(context);

    //#region /getPureCloud Token
    let dbToken = await db.getToken(context);
    if (dbToken) {
        context.log(`use token from database ${dbToken.token.substring(0, 10)}(...)`);
        pc.setToken(dbToken.token);
    } else {
        let resp = await pc.login(context);
        if (resp) {
            await db.updateToken(context, resp);
        } else {
            return {
                status: 400,
                body: {
                    "error": "failed to auth against PureCloud, veirfy OAuth settings"
                }
            };
        }

    }

    //#endregion /getPureCloud Token

    //#region /getDivisionIds that match division names from AzureAD
    var divisionIds;
    try {
        divisionIds = await pc.getDivisions(context, divisions);
        if (divisionIds.length == 0) {
            context.log('no divisions found. abort');
            return {
                status: 404,
                body: {
                    "error": "no divisions found"
                }
            };
        }
    } catch (error) {
        return error
    }

    //#endregion /getDivisionIds that match division names from AzureAD

    //#region /getPureCloudIds for dedicated divisionIds
    var queueIds;
    try {
        queueIds = await pc.getQueues(context, divisionIds, 25, 1);
        if (queueIds.length == 0) {
            context.log('no queues found. Abort');
            return {
                status: 404,
                body: {
                    "error": "no queues found"
                }
            };
        }
    } catch (error) {
        return error
    }

    //#endregion /getPureCloudIds for dedicated divisionIds


    var result;
    try {
        result = await pc.getAnalyticsConversationDetails(context, interval, queueIds, page);
    } catch (error) {
        return error
    }
    
    // END 
    return {
        result
    };

};
