/*
    FunctionName: getMediaInformation
    Return media information about recordings attached to the received conversationId.
*/

let pc = require('../shared/purecloud');
let db = require('../shared/db');
let jwt = require('../shared/jwt');


module.exports = async function (context, req) {
    context.log('getInteractionDetails started');

    let conversationId = req.body.conversationId;
    

    if (!conversationId || !req.body.token) {
        return {
            status: 400,
            body: {
                "error": "conversationId or token is missing"
            }
        };
    }

    context.log('Input params -----');
    context.log(`id_token    : ${req.body.token.substring(0, 10)}(...)`);
    context.log(`conversationId    : ${conversationId}`);
    
    let bToken = await jwt.validate(context, req.body.token);
    if (!bToken) {        
        return {
            status: 400,
            body: {
                "error": "your AzureAD token is invalid."
            }
        };
    }
    context.log(`azureAD token is valid. continue`);


    // initialize azure databaze
    await db.init(context);

    //#region /getPureCloud Token
    let dbToken = await db.getToken(context);
    if (dbToken) {
        context.log(`use token from database ${dbToken.token.substring(0, 10)}(...)`);
        pc.setToken(dbToken.token);
    } else {
        let resp = await pc.login(context);
        if (resp) {
            await db.updateToken(context, resp);
        } else {
            return {
                status: 400,
                body: {
                    "error": "failed to auth against PureCloud, veirfy OAuth settings"
                }
            };
        }

    }

    //#endregion /getPureCloud Token
   

    //#region /getConversationDetails
    var conversationDetails;
    try {
        conversationDetails = await pc.getRecordingMediaInformation(context, conversationId);
        

    } catch (error) {
        return error
    }

    //#endregion /getDivisionIds that match division names from AzureAD


    return {
        conversationDetails
    };

};
