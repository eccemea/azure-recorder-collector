# Azure recorder collector

Collections of Azure Functions that are responsible to download recordings from PureCloud and stream their content to the front-end.
Authentication is based on AzureAD. Before each request is process, received from AzureAD token is validated.

## Prerequsits

You should have AzureAD first as software authenticate against it to receive valid token (access_token and tokenId).

### Azure, New App Registration & settings

1. Sign in to https://portal.azure.com and navigate to ``Azure Active Directory``
2. Select ``App Registrations`` and select ``New registration``
3. Give it a name, select Supported account types and fill in ``Redirect URI`` address. This is the place where FronEnd will be hosted.
4. Registrer application
5. From Overview page, copy ApplicationId and TenantId, you will need them later
6. In ``Authentication`` page, under Implicit grant, select both checkboxes next to the ``access tokens`` and ``ID tokens``
7. In ``API Permissions`` select permissions required by the App, those permissions should include option to return with Token assigned groups to the authenticated user (User Read All?)

### Azure, Storage accounts setup

Azure Functions to work properly need access to the table and fileShare on azure storage. By default if objects do not exists function will create them in Azure Storge env. Default name for table is ``purecloud``

1.  To create new one, navigate to the ``storage accounts`` and click ``add new``
2.  Choose Basic storage, give it a name, fill in all required paramters like Subscription, resource group, location and as Account kind select ``StorageV2`` (application works also with v1 version)
3. Navigate to the newly created storage account and select ``Access keys``
4. Copy ``Connection string`` for key1 or key2, it will be used later for Azure Function configuration

### Azure Functions, installation and configuration

There are 3 Azure functions used in this solution.
*   getConversations - find appropriate conversation details and return results to the browser
*   getMediaInformation - collect media information related to the recordings for selected conversationId
*   getRecording Stream - download recording from PureCloud and expose it to the browser

Recommended way of deployment is via Visual Studio Code.

1.  git clone this repo (``git clone https://DanielSzlaski@bitbucket.org/eccemea/azure-recorder-collector.git``)
2.  navigate to the root directory and create new file local.settings.json with following content

```{
  "IsEncrypted": false,
  "Values": {    
    "FUNCTIONS_WORKER_RUNTIME": "node",
    "clientId": "10d(...)",
    "clientSecret": "dwfWGEzdPeGfYUiLeZXGR1GQcZyKc(...)",
    "env": "mypurecloud.ie",
    "AZURE_STORAGE_CONNECTION_STRING": "DefaultEndpointsProtocol=https;(...)",
    "VALIDATE_AZURE_TOKEN": "1"
  }
}
```

*   env /clientId / clientSecret - it's your OAuth credentials from PureCloud org
*   AZURE_STORAGE_CONNECTION_STRING - it's a connection string to the Azure Storge object, copied in step ``Azure, Storage accounts setup`` 
*   VALIDATE_AZURE_TOKEN - recommended value is ``1``. When ``0`` Functions do not check received Azure tokenId from browser requests

3.  In Visual Studio Code navigate to the ``Extensions`` and find & install Azure Functions extension.
4.  Follow the steps described in extension to finish pre / post installations steps.
5.  After succesfull installtion you should be able to view on left side Azure Functions icon.
6.  Click on it and there should be visible all deployments on your Azure subscription.
7.  Click on 3rd icon on the top of the screen (arrow up) - to publish your deployment
8.  When prompted give it a name (like recorderCollector), all functions are type of HttpTrigger authenticated on Function level.
9.  When succeed extend Application Settings section, right click on it and ``Upload Local Settings``. Confirm that you want to override exitsing one
10. That's all. Your Azure Funtions are deployed. To get their URI's navigate to Functions (Read Only) section and use right mouse click on each and select ``Copy Function URL`` - save it, and configure FrontEnd.


#### Other / Dev part

To login against Azure OpenID - navigate (example)

https://login.microsoftonline.com/785ce69c-90cf-4dc7-a882-eaf312d1d15d/oauth2/v2.0/authorize?
client_id=37a72847-7e72-4c35-8234-beca1d0f267e
&response_type=id_token token
&redirect_uri=http%3A%2F%2Flocalhost%3A5500%2Findex.html
&response_mode=fragment
&scope=openid profile email user.read&claim=groups
&state=12345
&nonce=678910