/*
    FunctionName: getRecordingStream
    Function download provided recording (from received conversationId and recordingId) to Azure storage fileShare. Next temp sasToken is created for this file and 
    attached to the new preparated URL that is returned to the Browser.
    sasToken is valid for defined amount of time. (15 min by default)
*/

let pc = require('../shared/purecloud');
let db = require('../shared/db');
let jwt = require('../shared/jwt');


module.exports = async function (context, req) {
    context.log('getRecordingStream started');

    let conversationId = req.body.conversationId;
    let recordingId = req.body.recordingId;

    if (!req.body.token || !conversationId || !recordingId) {
        return {
            status: 400,
            body: {
                "error": "one or more input params is missing. required params are (recordingId, conversationId, token)"
            }
        };
    }

    context.log('Input params -----');
    context.log(`id_token    : ${req.body.token.substring(0, 10)}(...)`);
    context.log(`recordingId   : ${recordingId}`);
    context.log(`conversationId    : ${conversationId}`);

    let bToken = await jwt.validate(context, req.body.token);
    if (!bToken) {
        return {
            status: 400,
            body: {
                "error": "your AzureAD token is invalid."
            }
        };
    }
    context.log(`azureAD token is valid. continue`);


    // initialize azure databaze
    try {
        await db.init(context);
    } catch (error) {
        return error
    }

    //#region /getPureCloud Token
    let dbToken = await db.getToken(context);
    if (dbToken) {
        context.log(`use token from database ${dbToken.token.substring(0, 10)}(...)`);
        pc.setToken(dbToken.token);
    } else {
        let resp = await pc.login(context);
        if (resp) {
            await db.updateToken(context, resp);
        } else {
            return {
                status: 400,
                body: {
                    "error": "failed to auth against PureCloud, veirfy OAuth settings"
                }
            };
        }

    }

    //#endregion /getPureCloud Token

    //#region /getRecordingUrl that match division names from AzureAD
    var recording;
    try {
        recording = await pc.getConversationRecordingUrl(context, conversationId, recordingId, 1)

    } catch (error) {
        context.log(error);
        return error
    }


    var localRecordingUrl;
    try {
        localRecordingUrl = await db.saveMediaFile(context, recording);

    } catch (error) {
        context.log(error);
        return error
    }

    let buff = new Buffer(localRecordingUrl);
    let base64data = buff.toString('base64');
    context.log('media: ', recording.media);


    context.res = {
        status: 200,
        body: {
            data: base64data,
            media: recording.media
        },
        headers: {
            "Content-Disposition": "inline"
        }
    };
    context.done();


};
